pkgname="jetos"
pkgver="0.0.1"
pkgrel="1"
pkgdesc="JetOS Desktop Environments"
arch=("x86_64")
depends=(
    ## i3 WM
    "i3-wm"
    "picom"
    "polybar"

    ## Utilities
    "blueberry"
    "bluez-utils"
    "ccls"
    "jq"
    "kitty"
    "lazygit"
    "network-manager-applet"
    "otf-font-awesome"
    "ripgrep"
    "rofi"
    "rustup"
    "tree"
    "ttf-jetbrains-mono"
    "udiskie"
    "vim"
    "virtualbox-host-modules-arch"
)
# source=("hello-world.sh")
# sha512sums=("SKIP")

package() {
    DOTFILES=${HOME}/projects/dotfiles

    ## Create projects/ directory
    mkdir -p ${HOME}/projects

    ## Check out the Dotfiles repo
    if [ ! -e ${DOTFILES} ]; then
        pushd ${HOME}/projects
        git clone http://gitlab.com/jtyler76/dotfiles.git
        popd
    fi

    # ## Install AUR packages
    # yay --noconfirm -S      \
    #     brave-bin           \
    #     git-delta-bin       \
    #     librewolf-bin       \
    #     neovim-nightly-bin


    ## --- General --- ##

    ## Link librewolf config
    rm -rf ${HOME}/.librewolf
    ln -s ${DOTFILES}/librewolf ${HOME}/.librewolf

    ## Link ZSH config
    rm -rf ${HOME}/.zshrc
    ln -s ${DOTFILES}/zshrc ${HOME}/.zshrc

    ## Install oh-my-zsh if it's not already installed
    if [ ! -e ${HOME}/.oh-my-zsh ]; then
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
    fi

    ## Link ZSH theme
    rm -rf ${HOME}/.oh-my-zsh/themes/jetyler.zsh-theme
    ln -s ${DOTFILES}/oh-my-zsh/themes/jetyler.zsh-theme ${HOME}/.oh-my-zsh/themes/jetyler.zsh-theme

    ## Link kitty config
    rm -rf ${HOME}/.config/kitty
    ln -s ${DOTFILES}/config/kitty ${HOME}/.config/kitty

    ## Link lazygit config
    rm -rf ${HOME}/.config/lazygit
    ln -s ${DOTFILES}/config/lazygit ${HOME}/.config/lazygit

    ## Link neovim config
    rm -rf ${HOME}/.config/nvim
    ln -s ${DOTFILES}/config/nvim ${HOME}/.config/nvim


    ## --- i3 --- ##

    ## Link weather API token
    rm -rf ${HOME}/.owm-key
    ln -s ${DOTFILES}/owm-key ${HOME}/.owm-key

    ## Link Xmodmap key mappings
    rm -rf ${HOME}/.Xmodmap
    ln -s ${DOTFILES}/Xmodmap ${HOME}/.Xmodmap

    ## Link picom config
    rm -rf ${HOME}/.config/picom
    ln -s ${DOTFILES}/config/picom ${HOME}/.config/picom

    ## Link i3 configs
    rm -rf ${HOME}/.config/i3
    ln -s ${DOTFILES}/config/i3 ${HOME}/.config/i3

    ## Link polybar config
    rm -rf ${HOME}/.config/polybar
    ln -s ${DOTFILES}/config/polybar ${HOME}/.config/polybar

    ## Link rofi config
    rm -rf ${HOME}/.config/rofi
    ln -s ${DOTFILES}/config/rofi ${HOME}/.config/rofi
}
